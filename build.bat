@echo off

mkdir release\
pushd release\
cl /MD /Fe"build.exe" ..\source\main.cpp ..\source\graphics\*.cpp ..\source\system\*.cpp /EHsc -FAsc -Zi -I C:\OpenGL\glm -I C:\OpenGL\glfw-3.2.1.bin.WIN64\include -I C:\VulkanSDK\1.1.73.0\Include /link /LIBPATH:C:\OpenGL\glfw-3.2.1.bin.WIN64\lib-vc2015 /LIBPATH:C:\VulkanSDK\1.1.73.0\Lib vulkan-1.lib gdi32.lib kernel32.lib user32.lib shell32.lib glfw3.lib
popd
