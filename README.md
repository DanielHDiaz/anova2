### What is this repository for? ###
* The ANOVA2 engine is the successor of the ANOVA engine. The ANOVA2 engine utilizes the Vulkan graphics API.
	* Relevant version numbers
		* ANOVA2: 0.0.0 	(Engine)
		* Vulkan: 1.1.73.0	(Graphics API)
		* GLFW: 3.2.1		(a cross-platform library for OpenGL and Vulkan development. Provides a simple API for creating windows, contexts and surfaces, receiving input and events)
		* GLM: 0.9.9-a2		(OpenGL Mathematics. May or may not be used in the future.)

### How do I get set up? ###
* This project is set up to build directly from the command line using Visual C++. To setup:
	* Run vcvarsall script in your command prompt to set VC++ environment variables.
	* Download the Vulkan SDK from https://vulkan.lunarg.com/sdk/home (the current version being used is 1.1.73.0).
		* Install the SDK in C:\ (this is where the build shell script will look for the SDK when it's run).
	* Download the GLFW library from http://www.glfw.org/download.html (the current version being used is 3.2.1)
		* Install this in C:\OpenGL (this is where the build shell script will look for it).
	* Download the GLM library from https://github.com/g-truc/glm/tags (the currenty version being used is 0.9.9-a2)
		* Install this in C:\OpenGL (this is where the build shell script will look for it).
	* Finally, run the build.bat file to compile the code and generate an executable.
		* To run the engine, execute the command "release\build.exe" from the command line.

### Contribution guidelines ###
* Style guidelines
	* Please use the Allman brace style. Why? Because this is my project and that style is more easy on my eyes.
	* Methods are Capitalized camel-case (i.e. ThisIsMyMethodId()).
	* Variable naming convention:
		* g: global variables (gVariable)
		* m: member variables (mVariable)
		* I: Interface classes (IClass)
		
### Who do I talk to? ###
* Repo owner or admin