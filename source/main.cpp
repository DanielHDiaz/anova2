#include "system/headers/Engine.h"

int main(int argc, char** argv)
{
	bool result;
	Engine* engine = new Engine();

	result = engine->Initialize();
	if (!result)
	{
		throw std::runtime_error("Failed to initialize engine");
		return EXIT_FAILURE;
	}

	try
	{
		engine->Run();
	}
	catch(const std::runtime_error& e)
	{
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	result = engine->Shutdown();
	if(!result)
	{
		throw std::runtime_error("Failed to shut down ANOVA2 engine properly");
		// TODO: We're exiting anyway so why bother returning a failure???
	}

	return 0;
}
