#include "headers/Engine.h"

const int WIDTH = 800;
const int HEIGHT = 600;

Engine::Engine()
{
	mGraphics = new Graphics();
	mDone = false;
}

Engine::~Engine()
{
	delete mGraphics;
}

/**
 * Run the main game loop. Loop will run until window is manually closed by either clicking the close button in the taskbar
 * or hitting the "esc" key on the keyboard.
 *
 * Returns: true upon successful exit of game loop.
 */
bool Engine::Run()
{
	// Game loop
	while (!glfwWindowShouldClose(mWindow) && glfwGetKey(mWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		glfwPollEvents();
		mGraphics->RenderFrame();
	}

	vkDeviceWaitIdle(mGraphics->mDevice);
	return true;
}

/**
 * Initialize all of the components of the engine (window, graphics, input, audio, etc...):
 * 		1) Initialize the window to display our graphics.
 * 		2) Initialize the Vulkan graphics drivers.
 */
bool Engine::Initialize()
{
	bool result;

	// Initialize the window.
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	mWindow = glfwCreateWindow(WIDTH, HEIGHT, "ANOVA2", nullptr, nullptr);
	glfwSetWindowPos(mWindow, 0, 30);

	// Initialize Vulkan drivers
	result = mGraphics->Initialize(mWindow);
	if(false == result)
	{
		std::cout << "Failed to initialize Vulkan drivers." << std::endl;
		return false;
	}

	return true;
}

/**
 * Shutting down all of the components of the engine (window, graphics, input, audio, etc...):
 * 		1) Shutdown the Vulkan graphics drivers.
 * 		2) Shutdown the window that displays the graphics.
 */
bool Engine::Shutdown()
{
	bool result;

	// Destroy Vulkan graphics instance
	result = mGraphics->Shutdown();
	if(!result)
	{
		std::cout << "Failed to shutdown Vulkan drivers properly." << std::endl;
		return false;
	}

	// Destroy window
	glfwDestroyWindow(mWindow);
	glfwTerminate();

	return true;
}
