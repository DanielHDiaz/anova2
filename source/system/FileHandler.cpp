#include "headers/FileHandler.h"

/**
 * Unlike earlier APIs, shader code in Vulkan has to be specified in a bytecode format as opposed to human-readable syntax like GLSL and
 * HLSL. This bytecode format is called SPIR-V and is designed to be used with both Vulkan and OpenCL (both Khronos APIs). It is a format
 * that can be used to write graphics and compute shaders. The advantage of using a bytecode format is that the compilers written by GPU
 * vendors to turn shader code into native code are significantly less complex.
 *
 * Returns: A byte array of the contents of the file.
 */
std::vector<char> FileHandler::ReadShader(const std::string& filename) 
{
	// We only need to read from file.
	std::ifstream file(filename, std::ios::ate | std::ios::binary);

	if (!file.is_open())
	{
		throw std::runtime_error("Failed to open shader file.");
	}

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);
	file.close();

	return buffer;
}
