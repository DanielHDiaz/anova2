#include <fstream>
#include <vector>

class FileHandler
{
    public:
        static std::vector<char> ReadShader(const std::string& filename);
        // TODO: Create method to read .obj files.

    private:
        // Disallow creating an instance of this object.
        FileHandler() { };
};
