#pragma once
#include "../../graphics/headers/Graphics.h"

class Engine
{
	public:
		Engine();
		~Engine();

		bool Run();
		bool Initialize();
		bool Shutdown();

	private:
		GLFWwindow* mWindow;
		bool mDone;

		Graphics* mGraphics;
};
